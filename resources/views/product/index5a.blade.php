@extends('layouts.app')


@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"></div>

                <div class="card-body">

			<form role="form" method="POST">
   {{ csrf_field()}}
   
 		<div class="form-group">
                    <label for="nama">Supplier</label>
                     <select class="form-control" name="supplier" id="" onchange="this.form.submit()" >
                      <option value="">Sila Pilih</option>
                      @foreach ($suppliers as $supplier)
                        <option value="{{$supplier->id}}" 
                          @if (old('supplier') == $supplier->id) selected @endif > {{$supplier->name}} </option>
                      @endforeach
                 
                     </select>
                  </div>
 </form>

		<table class="table">
			@foreach ($product as $pro)
				<tr>
				<td>{{$pro->name}}</td>
				<td>{{$pro->categorie->name}}</td>
				<td>{{$pro->supplier->name}}</td>
			</tr>
		       @endforeach
		</table>

                 </div>
            </div>
        </div>
    </div>
</div>




@endsection
