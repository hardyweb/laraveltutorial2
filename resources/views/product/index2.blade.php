@extends('layouts.app')


@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"></div>

                <div class="card-body">

		<table class="table">

			<thead>
				<th>Categori</th>
				<th>Count of Product</th>
				<th>Rm </th>
			</thead>

		@foreach ($categories as $categorie)

			<tr>
				<td>{{$categorie->name}}</td>
				<td>{{$categorie->products_count}}</td>
				<td>{{$categorie->products_sum_unitprice}}
			</tr>
		@endforeach

		<tr>
			<td></td>
			<td>{{$sumcount}}</td>
			<td>{{$sumunitprice}}</td>
		</tr>
		</table>

                 </div>
            </div>
        </div>
    </div>
</div>




@endsection
