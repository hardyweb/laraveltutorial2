@extends('layouts.app')


@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"></div>

                <div class="card-body">

		<table class="table">

			@foreach ($products as $product)
			<tr>
				<td>{{$product->name}}</td>
				<td>{{$product->categorie->name}}</td>
				<td>{{$product->supplier->name}}</td>
			</tr>
			@endforeach
		</table>

                 </div>
            </div>
        </div>
    </div>
</div>




@endsection
