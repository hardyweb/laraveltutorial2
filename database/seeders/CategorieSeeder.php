<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategorieSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            'name' => 'Computer',
	]);

	 DB::table('categories')->insert([
            'name' => 'Applience',
	 ]);

	 DB::table('categories')->insert([
            'name' => 'Stationery',
	 ]);
	  DB::table('categories')->insert([
            'name' => 'Clothing',
	  ]);
	 DB::table('categories')->insert([
            'name' => 'Music',
	 ]);

	 DB::table('categories')->insert([
            'name' => 'Furniture',
	 ]);
	 DB::table('categories')->insert([
            'name' => 'Printer',
	 ]);



    }
}
