<?php

namespace Database\Factories;


use App\Models\Supplier;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
	    return [

		    'name' => $this->faker->text,

		    'unitprice' => $this->faker->randomFloat(2, 0, 1000),
		    'quantity' =>  $this->faker->numberBetween($min=1, $max=30),
		    'categorie_id' => $this->faker->numberBetween($min = 1, $max = 7), 
		    'supplier_id' => $this->faker->numberBetween($min = 1, $max = 100),
		    
		    		    
                             ];
    }
}
