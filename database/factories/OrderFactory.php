<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class OrderFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [


		'product_id' => $this->faker->numberBetween($min=1,$max=10000),
		'amount' => random_int(1,10),
        ];
    }
}
