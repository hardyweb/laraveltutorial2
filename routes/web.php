<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	return view('welcome');
});

Auth::routes();


Route::group(['middleware' => ['auth']], function () {

	Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

	Route::get('/product', [App\Http\Controllers\ProductController::class, 'index']);
	Route::get('/product2', [App\Http\Controllers\ProductController::class, 'index2']);

	Route::get('/product3/{product}', [App\Http\Controllers\ProductController::class, 'index3']);

	Route::get('/product4', [App\Http\Controllers\ProductController::class, 'index4']);

	Route::post('/product5/{supplier}', [App\Http\Controllers\ProductController::class, 'index5a']);

	Route::get('/product5', [App\Http\Controllers\ProductController::class, 'index5']);


	Route::get('/orderreport', [App\Http\Controllers\OrderController::class, 'orderreport']);

	
});
