<?php

namespace App\Traits;

use App\Models\Product;

trait UmumTrait
{


	public function getData($model){

	
	return $model::all();
	
	}


	public function countProduct(){


	return Product::get()->count();
		
	}


	public function sumPrice()	{


	return Product::sum('unitprice');


		
	}

	public function sumPriceByCategorie($item):float 
	{

      		return Product::whereCategorieId($item)->sum('unitprice');

		

	}



}

