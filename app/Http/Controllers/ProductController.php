<?php

namespace App\Http\Controllers;

use App\Models\Categorie;
use App\Models\Product;
use App\Models\Supplier;
use App\Traits\UmumTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{

	use UmumTrait;
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{



		// $products = Product::all();
		// $products = Product::with('supplier','categorie')->orderBy('categorie_id')->get();

		$products = Product::select('id', 'name', 'supplier_id', 'categorie_id')->with('supplier:suppliers.id,name', 'categorie:categories.id,name')->orderBy('categorie_id')->get();



		return view('product.index', compact('products'));
	}

	public function index2()
	{

		$categories = Categorie::with('products')->withcount('products')->withsum('products', 'unitprice')->get();

		$sumunitprice = $categories->pluck('products')->collapse()->sum('unitprice');

		$sumcount = $categories->pluck('products')->collapse()->count();

		return view('product.index2', compact('categories', 'sumunitprice', 'sumcount'));


		/*
	    $data=[



		'sumunitprice' => $categories->pluck('products')->collapse()->sum('unitprice'),

		'sumcount' => $categories->pluck('products')->collapse()->count(),
	    ];

		 return view('product.index2',compact('categories'))->with($data);
 */
	}


	public function index3(Product $product)
	{

		//route model binding
		//

        //route model binding with relationship

        //https://stackcoder.in/posts/relationship-table-data-with-route-model-binding-in-laravel

		return view('product.index3', compact('product'));
	}



	public function index4()
	{



		$categories = $this->getData(new Categorie());

		$suppliers = $this->getData(new Supplier());


		$product = $this->countProduct();


		$sumprice = $this->sumPrice();

		$sumbycategori = $this->sumPriceByCategorie(2);


		dd($sumbycategori);
	}


	public function index5()
	{

		$suppliers = $this->getData(new Supplier());
		return view('product.index5', compact('suppliers'));
	}

	public function index5a(Request $request)
	{



		$product = Product::with('supplier', 'categorie')->whereSupplierId($request->supplier)->get();

		return view('product.index5a', compact('product'));
	}
}
