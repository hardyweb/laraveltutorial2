<?php

namespace App\Services;

use App\Models\Order;

class OrderService{

	public function getReport() {
	

		return Order::with('products')->withcount('products')->get();	
	
	}

	public function getReportByCategori(){
	
	
	
	 return \DB::table('invoice_product')
            ->select('products.name', \DB::raw('sum(invoice_product.price * invoice_product.quantity) as total_revenue'))
            ->join('products', 'invoice_product.product_id', '=', 'products.id')
            ->groupBy('products.name')
            ->orderBy('total_revenue', 'desc')
            ->get();
	
	}




}
