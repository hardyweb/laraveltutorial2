<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
	use HasFactory;



	public function order(){
	
	return $this->belongsTo(Order::class);
	
	}

	public function products(){

		return $this->hasMany(Product::class,'id','product_id');

	
	}

	public function supplier(){
	
		return $this->hasManyThrough(Product::class,Supplier::class,'id','supplier_id');

	}

	public function categorie(){
	
		return $this->hasManyThrough(Product::class,Categorie::class,'id','categorie_id');
	}
}
