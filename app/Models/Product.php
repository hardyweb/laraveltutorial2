<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;


    	public function categorie(){

	return $this->hasOne(Categorie::class,'id','categorie_id');

	}

    	public function supplier(){

	return $this->hasOne(Supplier::class, 'id','supplier_id');

	}

	public function order(){
	
	return $this->hasMany(Order::class);
	}
}
